﻿namespace WOK_SMAT.Pages
{
    class regexmatches
    {

        public static string MatchNumber()
        {
            return @"[+]*[2-4|0]*[\W]?[0]?[\W]?[7|8|9][0|1]\d{8}";


        }

        public static string MatchNumber1()
        {
            string re1 = "(\\+)";   // Any Single Character 1
            string re2 = "(\\d)";   // Any Single Digit 1
            string re3 = "(\\d)";   // Any Single Digit 2
            string re4 = "(\\d)";   // Any Single Digit 3
            string re5 = "(\\()";   // Any Single Character 2
            string re6 = "(\\d)";   // Any Single Digit 4
            string re7 = "(\\))";   // Any Single Character 3
            string re8 = "(\\d)";   // Any Single Digit 5
            string re9 = "(\\d)";   // Any Single Digit 6
            string re10 = "(\\d)";  // Any Single Digit 7
            string re11 = "(\\d)";  // Any Single Digit 8
            string re12 = "(\\d)";  // Any Single Digit 9
            string re13 = "(\\d)";  // Any Single Digit 10
            string re14 = "(\\d)";  // Any Single Digit 11
            string re15 = "(\\d)";  // Any Single Digit 12
            string re16 = "(\\d)";  // Any Single Digit 13
            string re17 = "(\\d)";	// Any Single Digit 14

            return re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 + re9 + re10 + re11 + re12 + re13 + re14 + re15 + re16 + re17;
        }


        public static string MatchNumber2()
        {
            string re1 = "(0)";   // Any Single Digit 1
            string re2 = "(\\d)";   // Any Single Digit 2
            string re3 = "(\\d)";   // Any Single Digit 3
            string re4 = "(\\d)";   // Any Single Digit 4
            string re5 = "(\\d)";   // Any Single Digit 5
            string re6 = "(\\d)";   // Any Single Digit 6
            string re7 = "(\\d)";   // Any Single Digit 7
            string re8 = "(\\d)";   // Any Single Digit 8
            string re9 = "(\\d)";   // Any Single Digit 9
            string re10 = "(\\d)";  // Any Single Digit 10
            string re11 = "(\\d)";  // Any Single Digit 11
            
            return re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 + re9 + re10 + re11;
        }


        public static string MatchNumber3()
        {
            string re1 = "(0)";   // Any Single Digit 1
            string re2 = "(\\d)";   // Any Single Digit 2
            string re3 = "(\\d)";   // Any Single Digit 3
            string re4 = "(\\d)";   // Any Single Digit 4
            string re5 = "(-)"; // Any Single Character 1
            string re6 = "(\\d)";   // Any Single Digit 5
            string re7 = "(\\d)";   // Any Single Digit 6
            string re8 = "(\\d)";   // Any Single Digit 7
            string re9 = "(-)"; // Any Single Character 2
            string re10 = "(\\d)";  // Any Single Digit 8
            string re11 = "(\\d)";  // Any Single Digit 9
            string re12 = "(\\d)";  // Any Single Digit 10
            string re13 = "(\\d)";	// Any Single Digit 11


            return re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 + re9 + re10 + re11 + re12 + re13;
        }

        public static string MatchNumber4()
        {
            string re1 = "(\\+)";   // Any Single Character 1
            string re2 = "(\\2)";   // Any Single Digit 1
            string re3 = "(\\3)";   // Any Single Digit 2
            string re4 = "(\\4)";   // Any Single Digit 3
            string re5 = "(.)"; // Any Single Character 2
            string re6 = "(.)"; // Any Single Character 3
            string re7 = "(\\d)";   // Any Single Digit 4
            string re8 = "(\\d)";   // Any Single Digit 5
            string re9 = "(\\d)";   // Any Single Digit 6
            string re10 = "(.)";    // Any Single Character 4
            string re11 = "(.)";    // Any Single Character 5
            string re12 = "(\\d)";  // Any Single Digit 7
            string re13 = "(\\d)";  // Any Single Digit 8
            string re14 = "(\\d)";  // Any Single Digit 9
            string re15 = "(.)";    // Any Single Character 6
            string re16 = "(.)";    // Any Single Character 7
            string re17 = "(\\d)";  // Any Single Digit 10
            string re18 = "(\\d)";  // Any Single Digit 11
            string re19 = "(\\d)";  // Any Single Digit 12
            string re20 = "(\\d)";	// Any Single Digit 13

            return re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 
                + re9 + re10 + re11 + re12 + re13 + re14 + re15 + re16 + re17 + re18 + re19 + re20;
        }

        public static string MatchNumber5()
        {
            string re1 = "(\\0)";   // Any Single Digit 1
            string re2 = "(\\d)";   // Any Single Digit 2
            string re3 = "(\\d)";   // Any Single Digit 3
            string re4 = "(\\d)";   // Any Single Digit 4
            string re5 = "(.)"; // Any Single Character 1
            string re6 = "(\\d)";   // Any Single Digit 5
            string re7 = "(\\d)";   // Any Single Digit 6
            string re8 = "(\\d)";   // Any Single Digit 7
            string re9 = "(.)"; // Any Single Character 2
            string re10 = "(\\d)";  // Any Single Digit 8
            string re11 = "(\\d)";  // Any Single Digit 9
            string re12 = "(\\d)";  // Any Single Digit 10
            string re13 = "(\\d)";	// Any Single Digit 11

            return re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 + re9 + re10 + re11 + re12 + re13;
        }
    }
}
